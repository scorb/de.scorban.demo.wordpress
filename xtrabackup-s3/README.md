## XtraBackup streaming to AWS S3
This Docker image contains an installation of [XtraBackup](https://github.com/percona/percona-xtrabackup) sitting on Ubuntu 16.04. Use it to create encrypted (AES256) backups of your MariaDB / MySQL database containers. Backup files will be compressed and streamed to AWS S3 bucket using [gof3r](https://github.com/rlmcpherson/s3gof3r) client.


# Prepare backup database user
Grant backup-required privileges for the backup user (here **backupuser**):
`GRANT RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'backupuser'@'192.%' IDENTIFIED by 'topsecret'`


# Generate encryption key
Create an AES encryption key file with openssl. Use **echo -n** to ensure that no extra characters get added:
`echo -n 'openssl rand -base64 24' > keyfile`


# Environment Variables
HOST - IP address database is running on  
PORT - database port (default 3306)  
USER - backup user name  
PASSWORD - backup user password
ENCRYPT_KEY - encryption key
AWS\_ACCESS\_KEY_ID - AWS credentials for S3 bucket
AWS\_SECRET\_ACCESS_KEY - AWS credentials for S3 bucket
AWS_REGION - AWS region for S3 bucket
AWS\_S3\_BUCKET - S3 bucket name
AWS\_S3\_FILENAME - name for backup file


# Usage - Backup
Mount (database) input directory to **/var/lib/mysql**.
Example:
`docker run -it --rm -v mariadb-volume:/var/lib/mysql
-e HOST=192.168.178.100
-e USER=backupuser
-e PASSWORD=backupuserpw
-e ENCRYPT_KEY=topsecret
-e AWS_ACCESS_KEY_ID=AWSKEYSFODNN7EXAMPLE
-e AWS_SECRET_ACCESS_KEY=awsSecReTblaBlablaBlaExaMplesEcreTKey
-e AWS_REGION=eu-central-1
-e AWS_S3_BUCKET=myBucket
-e AWS_S3_FILENAME=mariadb/backup.xbcrypt
scorb/xtrabackup-s3


# How to restore
1. Download your backup file (here *mariadb/backup.xbcrypt*) from S3 bucket.
2. Decrypt it using same ENCRYPT_KEY and extract the xbstream to **restoredir**. This must be an existing directory! 
`xtrabackup --encrypt-algo=AES256 --decrypt --encrypt-key=topsecret | xbstream -x -C restoredir`
3. Decompress files:
`xtrabackup --decompress restoredir`
4. Replay the logs:
`xtrabackup --replay-log restoredir`
5. Copy back the backup files to your existing database directory or mount them on a new instance.