##### PART I #####
### Values will be read from environment variables (TF_VAR_access_key, TV_VAR_secret_key) ###
variable "access_key" {}
variable "secret_key" {}

provider "aws" {
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
    region = "eu-central-1"
}


##### PART II #####
### Open Ports: 22 (SSH) IN, 80 (HTTP) IN, all OUT ###
resource "aws_security_group" "demo_wordpress-sg" {
    name = "demo_wordpress-sg"
    description = "ssh + http inbound"
    ingress {
        from_port = 22
        to_port = 22
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags {
        Name = "demo_wordpress-sg"
    }
}


##### PART III #####
### Create AWS EC2 instance ###
resource "aws_instance" "demo_wordpress" {
    ami = "ami-87564feb"
    availability_zone = "eu-central-1a"
    instance_type = "t2.micro"
    key_name = "AWS-wordpress-key"   # SSH key name to use for SSH-connection for this instance
    security_groups = ["${aws_security_group.demo_wordpress-sg.name}"]
    tags {
        Name = "demo_wordpress"
    }
    ##### PART IV #####
    provisioner "remote-exec" {   # create folder /bootstrap on AWS instance
        connection {
            host = "${aws_instance.demo_wordpress.public_ip}"
            private_key = "C:/.../.ssh/AWS-wordpress-key.pem"   # path to your private SSH key (AWS-wordpress.pem)
            user = "ubuntu"
        }
        inline = [
            "sudo mkdir /bootstrap",
            "sudo chown ubuntu /bootstrap",
            "sudo chgrp ubuntu /bootstrap"
        ]
    }
    provisioner "file" {   # copy content of folder /bootstrap to AWS instance /bootstrap
        connection {
            host = "${aws_instance.demo_wordpress.public_ip}"
            private_key = "C:/.../.ssh/AWS-wordpress-key.pem"   # path to your private SSH key (AWS-wordpress.pem)
            user = "ubuntu"
        }
        source = "bootstrap"
        destination = "/"
    }
    provisioner "remote-exec" {   # execute bootstrap.sh on AWS instance
        connection {
            host = "${aws_instance.demo_wordpress.public_ip}"
            private_key = "C:/.../.ssh/AWS-wordpress-key.pem"   # path to your private SSH key (AWS-wordpress.pem)
            user = "ubuntu"
        }
        script = "bootstrap.sh"
    }
}