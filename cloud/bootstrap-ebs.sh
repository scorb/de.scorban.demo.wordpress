#!/usr/bin/env bash
INTERNET_CONNECTED=0


echo "##### Testing internet connection #####"
while [ ${INTERNET_CONNECTED} == 0 ]
do
    echo "### Ping 'google.com' to check if we have an internet connection ..."
    ping google.com -c 4 >/dev/null && INTERNET_CONNECTED=1 && break
    echo "### Ping was unsuccessful - sleeping 5s before retry ..."
    sleep 5
done
echo "### Ping was successful - starting bootstrap now!"


echo "##### Mount Volume #####"
if [ "$(sudo file -s /dev/xvdh)" = "/dev/xvdh: data" ]
then
    echo "### Formatting new volume ###"
    sudo mkfs -t ext4 /dev/xvdh
fi
sudo mkdir /data
sudo mount /dev/xvdh /data
sudo sed -i '$a/dev/xvdh   /data       ext4    defaults,nofail 0   2' /etc/fstab


echo "##### Install Docker Prerequisites #####"
# Update package information, ensure that APT works with the https method, and that CA certificates are installed.
sudo apt-get -y update
sudo apt-get -y install apt-transport-https ca-certificates
# Add the new GPG key.
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo cp /bootstrap/docker.list /etc/apt/sources.list.d/docker.list
sudo apt-get -y update
sudo apt-get -y purge lxc-docker
# Install "linux-image-extra" kernel package and "apparmor"
sudo apt-get -y install linux-image-extra-$(uname -r)
sudo apt-get -y install apparmor


echo "##### Link Docker volumes to /data/docker #####"
if ! [ -d "/data/docker" ]
then
    sudo mkdir -p /data/docker/volumes
    sudo chmod -R 700 /data/docker
fi
sudo mkdir /var/lib/docker
sudo ln -s /data/docker/volumes /var/lib/docker/volumes


echo "##### Install and start Docker #####"
sudo apt-get -y install docker-engine


echo "##### Install Docker Compose #####"
sudo apt-get -y install python-pip
sudo pip install docker-compose


echo "##### Install and start docker container #####"
cd /bootstrap
sudo docker-compose up -d