#!/usr/bin/env bash
INTERNET_CONNECTED=0

echo "##### Testing internet connection #####"
while [ ${INTERNET_CONNECTED} == 0 ]
do
    echo "### Ping 'google.com' to check if we have an internet connection ..."
    ping google.com -c 4 >/dev/null && INTERNET_CONNECTED=1 && break
    echo "### Ping was unsuccessful - sleeping 5s before retry ..."
    sleep 5
done
echo "### Ping was successful - starting bootstrap now!"


echo "##### Install Docker #####"
sudo yum install -y docker
sudo service docker start
sudo chkconfig docker on


echo "##### Start docker container #####"
sudo docker run -d -p 25565:25565 --name minecraft scorb/demo_wp_minecraft
