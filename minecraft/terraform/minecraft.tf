### Values will be read from environment variables (TF_VAR_access_key, TV_VAR_secret_key) ###
variable "access_key" {}
variable "secret_key" {}

provider "aws" {
    access_key = "${var.access_key}"
    secret_key = "${var.secret_key}"
    region = "eu-central-1"
}


### Open Ports: 22 (SSH) IN, 80 (HTTP) IN, all OUT ###
resource "aws_security_group" "minecraft-sg" {
    name = "minecraft-sg"
    description = "ssh + minecraft inbound"
    ingress {
        from_port = 22
        to_port = 22
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 25565
        to_port = 25565
        protocol = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags {
        Name = "minecraft-sg"
    }
}


### Create AWS EC2 instance ###
resource "aws_instance" "minecraft" {
    ami = "ami-d3c022bc"
    availability_zone = "eu-central-1a"
    instance_type = "t2.micro"
    key_name = "AWS-minecraft-key"   # SSH key name to use for SSH-connection for this instance
    security_groups = ["${aws_security_group.minecraft-sg.name}"]
    tags {
        Name = "minecraft"
    }
    provisioner "remote-exec" {
        connection {
            host = "${aws_instance.minecraft.public_ip}"
            private_key = "C:/.../.ssh/AWS-minecraft-key.pem"   # path to your private SSH key (AWS-minecraft.pem)
            user = "ec2-user"
        }
        script = "bootstrap.sh"
    }
}
