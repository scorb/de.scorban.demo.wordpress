## XtraBackup Dockerized
This Docker image contains an installation of [XtraBackup](https://github.com/percona/percona-xtrabackup) sitting on Ubuntu 16.04. Use it to create easy backups of your MariaDB / MySQL database containers.

# Prepare backup database user
Grant backup-required privileges for the backup user (here **backupuser**):
`GRANT RELOAD, PROCESS, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'backupuser'@'192.%' IDENTIFIED by 'topsecret'`

# Environment Variables
HOST - IP address database is running on  
PORT - database port (default 3306)  
USER - backup user name  
PASSWORD - backup user password

# Usage
Mount (database) input directory to **/var/lib/mysql** and (backup) output directory into the container's **/output** directory.
Example:
`docker run -it --rm -v mariadb-volume:/var/lib/mysql -v C:/temp/backup:/output -e HOST=192.168.178.100 -e USER=backupuser -e PASSWORD=topsecret scorb/xtrabackup`